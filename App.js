//LIBRARIES
import React, { Component } from 'react';
import AppNavigation from './src/Navigation/AppNavigation'
import { Provider } from 'react-redux'
import Store from './src/Redux/Store'
//ASSETS
import firebaseApp from './src/Config/Firebase/FirebaseConfig'

export default class App extends Component {

  componentWillMount() {
      <firebaseApp />
        console.disableYellowBox = true;
  }
  render() {
    return (
      <Provider store={Store}>
        <AppNavigation />
      </Provider>
    );
  }
}

