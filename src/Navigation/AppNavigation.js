//LIBRARIES
import React, { Component } from 'react'
import { View,StyleSheet } from 'react-native'
//ASSETS
import AppNavigator from './AppNavigationConfiguration'

//======CLASS DECLARATION========//
export default class AppNavigation extends Component {

    render() {
            return(      
                    <AppNavigator  />       
                )
            }
}

//=====STYLES DECLARATION======//
const styles = StyleSheet.create({
    container: {
        flex:1
    }
})
 
