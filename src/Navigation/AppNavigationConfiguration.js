 //LIBRARIES
 import React from 'react'
 import { TabNavigator, StackNavigator } from 'react-navigation'
 import Icon from 'react-native-vector-icons/Feather'
 //ASSETS
 import { Colors } from '../Assets'
 import AuthNavigation from '../AuthTab/AuthTabNavigation'
 import MainTab1Nav from '../MainTab1/MainTab1Navigation'
 import MainTab2Nav from '../MainTab2/MainTab2Navigation'
 import SettingsTabNav from '../SettingsTab/SettingsTabNavigation'
 //=======TABS DECLARATION=========//

const TabBarConfig = TabNavigator({
    TAB1 : { screen : MainTab1Nav,
        navigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ tintColor, focused }) => (
              <Icon name='radio' size={22} tintColor={Colors.HEADER} />
            ),
            configureTransition: () => ({ screenInterpolator: () => null }),
          })
    },
    TAB2: { screen : MainTab2Nav,
        navigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ tintColor, focused }) => (
              <Icon name='layers' size={20} tintColor={Colors.HEADER} />
            ),
            configureTransition: () => ({ screenInterpolator: () => null }),
          })
    },
    Settings: { screen : SettingsTabNav,
        navigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ tintColor, focused }) => (
              <Icon name='settings' size={20} tintColor={Colors.HEADER} />
            ),
            configureTransition: () => ({ screenInterpolator: () => null }),
          })
    }
},
{
    navigationOptions: {
        header: null,
    },
    tabBarOptions: {
        activeBackgroundColor: Colors.TABBAR,
        inactiveBackgroundColor: Colors.TABBAR,
        activeTintColor: Colors.HEADER,
        inactiveTintColor: Colors.HEADER,
        labelStyle: { fontSize: 15 },
        style: {
            height: 52,
        }
    }
})

 //=======APP STACK DECLARATION=========//

const AppNavigator = StackNavigator({
    TabBar : { screen : TabBarConfig },
    Auth : { screen : AuthNavigation },
    
   
},{
navigationOptions:{
    initialRouteName: 'Auth',
    header: null
}
})


export default AppNavigator
