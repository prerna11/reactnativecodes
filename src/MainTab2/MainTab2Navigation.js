 //LIBRARIES
 import { StackNavigator } from 'react-navigation'
 //ASSETS
 import Screen1View from './Screen1/View'


 //=========STACK DECLARATION FOR MAINTAB 2 SCREENS===========//


 const MainTab2Nav = StackNavigator({
    Screen1 : { screen : Screen1View  }
 });

 export default MainTab2Nav;