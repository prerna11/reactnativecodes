//LIBRARIES
import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import { Colors } from '@Assets'
//====CLASS DECLARATION====//
export default class Screen1View extends Component {

    //Specify Navigation Properties for screen
    static navigationOptions = () => ({              
        headerTitle : 'SCREEN 2',
        headerStyle : {backgroundColor: Colors.HEADER },
        headerTintColor: Colors.WHITE,
        gesturesEnabled:false
    })   

    render() {
        return(
            <View>
                <Text> TAB 2 VIEW SCREEN 1 </Text>
            </View>    
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor:'red'
    },
    Text: {
        color: 'blue'
    }
})
