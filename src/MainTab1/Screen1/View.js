//LIBRARIES
import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'
//Assets
import { Colors } from '@Assets'
import { RoundedButton, ButtonwIcon } from '../../Components';

//====CLASS DECLARATION====//
export default class Screen1View extends Component {


 //--->>>Specify Navigation Properties for screen------>>>
    static navigationOptions = () => ({              
        headerTitle : 'SCREEN 1',
        headerStyle : {backgroundColor: Colors.HEADER },
        gesturesEnabled:false,
        headerTintColor:Colors.WHITE ,
    })   

//--->>>LifeCycle Methods------>>>
componentWillMount() {

}

componentWillReceiveProps(nextProps) {

}

componentDidMount() {

}

componentWillUnmount() {

}

//--->>>Render Method------>>>

    render() {
        return(
            <View style={ styles.container }>
                <RoundedButton 
                    title="Rounded Button"
                    roundedButtonStyle={ styles.roundedButtonStyle }
                    onPress={() => alert('Pressed') }
                />  

                <ButtonwIcon
                    title="With Icon"
                    buttonwIconStyle={ styles.buttonwIconStyle }
                    onPress={ () => alert('Whooo Icon pressed') }
                />  
            </View>    
        )
    }
}

//======STYLES DECLARATION======//

const styles = StyleSheet.create({
    container: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    roundedButtonStyle: {
        marginTop: 30
    },
    buttonwIconStyle: {
        marginTop: 30
    }
})
