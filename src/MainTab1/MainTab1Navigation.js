 //LIBRARIES
 import { StackNavigator } from 'react-navigation'
 //ASSETS
 import Screen1View from './Screen1/View'


 //=========STACK DECLARATION FOR MAINTAB 1 SCREENS===========//

 const MainTab1Nav = StackNavigator({
    Screen1 : { screen : Screen1View  }
 });

 export default MainTab1Nav;