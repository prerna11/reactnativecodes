 //LIBRARIES
 import { StackNavigator } from 'react-navigation'
 //ASSETS
 import Login from './Login/View'
 import SignUp from './SignUp/View'

 //=========STACK DECLARATION FOR MAINTAB 1 SCREENS===========//

 const AuthNavigation = StackNavigator({
    Login : { screen : Login  },
    SignUp : { screen : SignUp },
 });

 export default AuthNavigation;