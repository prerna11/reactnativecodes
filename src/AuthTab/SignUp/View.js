//LIBRARIES
import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native'
import { Field, reduxForm, reset, change } from 'redux-form'
import Icon from 'react-native-vector-icons/Feather'
import { connect } from 'react-redux'

//Assets
import { Colors } from '../../Assets'
import { ClassControllers } from './Controllers'
import { ReduxFormTextField, RequiredValidation, RequiredEmailValidation, EmailValidation, PasswordValidation, SimpleButton, LoadWheel } from '../../Components'
import { signUpRequest } from '../../Redux/Actions/AuthActions'

//====CLASS DECLARATION====//
 class SignUp extends Component {

 //------->>>Specify Navigation Properties for screen---------->>>
    static navigationOptions = () => ({              
       header: null
    })   


//--------->>>State Initilization----------->>>
state = {
    email : '',
    password:'',
    focusField: '',
    emailFocused: false,
    passwordFocused: false,
    securePassword: true,
    isLoading: false
    
}

//------------>>>LifeCycle Methods------------->>>

componentWillMount() {

}

componentWillReceiveProps(nextProps) {
    console.log(nextProps.auth)
    if(nextProps.auth.signUpSuccess && this.state.isLoading) {
        this.setState({ isLoading: false })
        this.props.navigation.navigate('Login')
    } 
    else if (nextProps.auth.signUpSuccessFail&& this.state.isLoading) {
        this.setState({ isLoading: false })
        alert('User already exist')
    }
}

componentDidMount() {

}

componentWillUnmount() {

}

//------------->>>Controllers/Functions------------>>>>

onSignUpSubmit() {
    console.log(this.state.email),
    console.log(this.state.password),
    //Keyboard.dismiss();
    this.setState({ isLoading: true });
    this.props.signUpRequest({ email: this.state.email, password: this.state.password })

  }


//----------->>>Render Method-------------->>>

    render() {
        const { handleSubmit } = this.props
        return (
            <View style = { styles.container }>
                <View style = { styles.content }>
                    <View style = { styles.fieldView }>
                        <Icon 
                            name = "mail" 
                            style = { styles.emailIcon } 
                            color = { Colors.WHITE } 
                            size = { 25 } 
                        />
                        <Field
                            name = "Email"
                            label = 'Email'
                            labelHeight = { 16 }
                            textColor = { Colors.WHITE }
                            baseColor = { Colors.WHITE }
                            focus
                            withRef
                            ref = { (componentRef) => this.Email = componentRef }
                            refField = "Email"
                            onSubmitEditing = { () => { this.Password.getRenderedComponent().refs.Password.focus() } }
                            component = { ReduxFormTextField }
                            onFocus={ (event) => this.setState({ emailFocused: true, focusField: 'Email' }) }
                            onBlur = { (event) => this.setState({ emailFocused: false }) }
                            renderAccessory = { () => this.state.emailFocused ? <TouchableOpacity onPress={ () => this.Email.getRenderedComponent().refs.Email.clear() }><Icon name="x" size={16} color={Colors.WHITE} /></TouchableOpacity> : undefined }
                            returnKeyType = { 'next' }
                            secureTextEntry = { false }
                            value = { this.state.email }
                            onChange = { (event, email) => this.setState({ email: email }) }
                            validate={ [ RequiredValidation, EmailValidation ] }
                        />
                    </View>
                    <View style = { styles.fieldView }>
                        <Icon
                            name = "lock" 
                            style = { styles.passwordIcon } 
                            color =  { Colors.WHITE }
                            size = { 25 } 
                        />
                        <Field
                            name = "Password"
                            label = 'Password'
                            withRef
                            refField = 'Password'
                            ref = { (componentRef) => this.Password = componentRef }
                            textColor = { Colors.WHITE }
                            baseColor = { Colors.WHITE }
                            component = { ReduxFormTextField }
                            labelHeight = { 16 }
                            onFocus = { (event) => this.setState({ passwordFocused: true, focusField: 'Password' }) }
                            onBlur = { (event) => this.setState({ passwordFocused: false }) }
                            renderAccessory = { () => this.state.passwordFocused
                                ?
                                ( this.state.securePassword ? <TouchableOpacity onPress = { () => this.setState({ securePassword: false }) }><Icon name = "eye" size = { 16 } color = { Colors.WHITE } /></TouchableOpacity>
                                : <TouchableOpacity onPress = { () => this.setState({ securePassword: true }) }><Icon name = "eye-off" size = { 16 } color = { Colors.WHITE } /></TouchableOpacity> )
                                : undefined }
                            returnKeyType = { 'done' }
                            // onSubmitEditing={handleSubmit(() => {this.onLoginSubmit(); Keyboard.dismiss() })}
                            secureTextEntry = { this.state.securePassword }
                            onChange = { (event, password) => this.setState({ password: password }) }
                            value = { this.state.password }
                            validate = { [ RequiredValidation, PasswordValidation ] }
                        />
                    </View>

                    { this.state.isLoading ? <LoadWheel /> : null }

                    <View style = { styles.signUpButtonView }>                 
                        <SimpleButton
                                title= "SIGNUP"
                                onPress={handleSubmit(this.onSignUpSubmit.bind(this))}
                        />
                    </View>  
                </View>  
                              
            </View>    
        )
    }
}


//--->>>Form Binding------>>>
SignUp = reduxForm({
    // a unique name for the form
    form: 'signup',
  })(SignUp)

//Props Connection
    const mapStateToProps = ( state ) => {
    return {
        auth : state.Auth
    }
  }

//--->>>Redux Connection------>>>
  export default connect(mapStateToProps ,{ signUpRequest })(SignUp);

//======STYLES DECLARATION======//

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: Colors.HEADER,
    },
    content:{
        flex:1,
        alignItems: 'center',
        justifyContent:'center',
        alignSelf:'center'
    },
    fieldView: {
        flexDirection: 'row'
    },
    emailIcon: {
        marginTop: 18,
        marginRight: 10,
    },
    passwordIcon: {
        marginTop: 15,
        marginRight: 10, 
    },
    signUpButtonView:{
       marginTop:30
    }
   
})
