import { put, takeEvery } from 'redux-saga/effects';
import * as firebase from 'firebase';
import { 
    SIGN_UP_SUCCESS,
    SIGN_UP_FAIL,
    SIGN_UP_REQUESTING
 } from '../Types/AuthTypes'

 /************************ Signup function ****************************/


export const watchSignUpAsync = function* watchSignUpAsync({ params }) {
 console.log(params)
 console.log('---------------SAGA CALLING')    
    try {
        console.log('---------------SAGA CALLING')

        yield firebase.auth().createUserWithEmailAndPassword(params.email, params.password)
        yield put({ type: SIGN_UP_SUCCESS });
    }   
    catch (e) {
       // alert(e)
        yield put({ type: SIGN_UP_FAIL });
    }     
}



const watchSignUp = function* watchSignUp() {
    yield takeEvery(SIGN_UP_REQUESTING, watchSignUpAsync);
  }

  export default watchSignUp;