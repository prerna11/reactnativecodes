import { put, takeEvery } from 'redux-saga/effects';
import * as firebase from 'firebase';
import { 
    LOGOUT_USER_SUCCESS,
    LOGOUT_USER_FAIL,
    LOGOUT_USER_REQUESTING
 } from '../Types/AuthTypes'

 /************************ Logout function ****************************/


export const watchLogoutAsync = function* watchLogoutAsync() {
 
 console.log('---------------SAGA CALLING')    
    try {
        console.log('---------------SAGA CALLING')

        yield firebase.auth().signOut()
        yield put({ type: LOGOUT_USER_SUCCESS });
    }   
    catch (e) {
       // alert(e)
        yield put({ type: LOGOUT_USER_FAIL });
    }     
}


const watchLogout = function* watchLogout() {
    yield takeEvery(LOGOUT_USER_REQUESTING, watchLogoutAsync);
  }

  export default watchLogout;