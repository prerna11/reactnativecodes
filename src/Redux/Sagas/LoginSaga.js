import { put, takeEvery } from 'redux-saga/effects';
import * as firebase from 'firebase';
import { 
    LOGIN_USER_SUCCESS,
    LOGIN_USER_REQUESTING,
    LOGIN_USER_FAIL
 } from '../Types/AuthTypes'

/************************ Login function ****************************/


export const watchLoginAsync = function* watchLoginAsync({ params }) {
 console.log(params)
 console.log('---------------SAGA CALLING')    
    try {
        console.log('---------------SAGA CALLING')

       yield firebase.auth().signInWithEmailAndPassword(params.email, params.password)
       yield put({ type: LOGIN_USER_SUCCESS });
    }   
    catch (e) {
       // alert(e)
        yield put({ type: LOGIN_USER_FAIL });
    }     
}


const watchLogin = function* watchLogin() {
    yield takeEvery(LOGIN_USER_REQUESTING, watchLoginAsync);
  }

  export default watchLogin;