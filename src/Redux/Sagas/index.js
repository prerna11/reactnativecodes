import watchSignUp from './SignUpSaga';
import watchLogin from './LoginSaga'
import watchLogout from './SettingsTabSaga'
const rootSaga = function* rootSaga() {
    yield [
      watchSignUp(),
      watchLogin(),
      watchLogout()
    ]
  };
  export default rootSaga;
  