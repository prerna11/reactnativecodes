import {
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAIL,
    SIGN_UP_FAIL,
    SIGN_UP_SUCCESS,
} from '../Types/AuthTypes'

const INITIAL_STATE = {
    email: null,
    password: '',
  };
  
    export default (state = INITIAL_STATE, action) => {
        console.log('*****************REDUCER')
        switch (action.type) {

            case LOGIN_USER_SUCCESS:
                return { loginSuccess: true };

            case LOGIN_USER_FAIL:
                return { loginSuccessFail: true };


            case SIGN_UP_SUCCESS:
                return { signUpSuccess: true };

            case SIGN_UP_FAIL:
                return { signUpSuccessFail: true };

            default:
                return state;
        };
    };