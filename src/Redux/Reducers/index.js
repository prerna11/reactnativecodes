import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import SettingsReducer from './SettingsTabReducer'
import { reducer as formReducer } from 'redux-form';

 let rootReducer = combineReducers({
  Auth: AuthReducer,
  form: formReducer,
  Settings: SettingsReducer

});
export default rootReducer