import {
    LOGOUT_USER_FAIL,
    LOGOUT_USER_SUCCESS
} from '../Types/AuthTypes'



const INITIAL_STATE = {
    email: null,
    password: '',
  };
  
export default (state = INITIAL_STATE, action) => {
    console.log('*****************REDUCER')
    switch (action.type) {

        case LOGOUT_USER_SUCCESS:
            return { logoutSuccess: true };
            
        default:
            return state;    
    }
}