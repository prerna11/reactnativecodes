//LIBRARIES
import React, { Component } from 'react'
import { Text, View, StyleSheet, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import { Avatar } from 'react-native-elements'
// import ImagePicker from 'react-native-image-crop-picker'
import _ from 'lodash'
import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements'

//ASSETS
import { Colors } from '../../Assets'
import { SimpleButton, LoadWheel, RoundedButton } from '../../Components'
import { logOutRequest } from '../../Redux/Actions/AuthActions'
import { Alert } from '../../Components/Alerts'
import { ReduxFormTextField, RequiredValidation } from '../../Components'
import { TextField } from 'react-native-material-textfield';


//====CLASS DECLARATION====//
class SettingsScreen extends Component {

//--------->>>Specify Navigation Properties for screen--------->>>

    static navigationOptions = () => ({              
        headerTitle : 'SCREEN 3',
        headerStyle : { backgroundColor: Colors.HEADER },
        headerTintColor: Colors.WHITE,
        gesturesEnabled:false
    })   

//--------->>>State Initilization----------->>>
    state = {
        isLoading: false,
        firstName: '',
        lastName : '',
        AddPhotoOptions:false,
        image:'',
        EditPhotoOptions:false
    }

//------------>>>LifeCycle Methods------------->>>

   async componentWillMount() {
        // let FirstName =  await AsyncStorage.getItem('FirstName')
        // let LastName =  await AsyncStorage.getItem('LastName')
        // let Address =  await AsyncStorage.getItem('Address')

        // this.setState({ 
        //     firstName : FirstName,
        //     lastName : LastName,
        //     address : Address
        //  })
    }

    componentWillReceiveProps(nextProps) {
        //console.log(nextProps.auth)
        if( nextProps.settings.logoutSuccess && this.state.isLoading ) {
            this.setState({ isLoading : false })
            this.props.navigation.navigate('Auth')
        } else if( nextProps.settings.logoutSuccessFail && this.state.isLoading ) {
            this.setState({ isLoading : false })
            alert('Username or password incorrect')
        }
    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

//------------->>>Controllers/Functions------------>>>>

    // onLogoutSubmit() {
    //     //Keyboard.dismiss();
    //     this.setState({ isLoading: true });
    //     this.props.logOutRequest()

    // }

    // onSavePress() {
    //     alert('Data has been saved')
    //     // console.log( this.state.firstName )
    //     // FirstName = await this.state.firstName 
    //     // LastName = await this.state.lastName
    //     // Address = await this.state.address
        
    //     // AsyncStorage.setItem('FirstName', FirstName)
    //     // AsyncStorage.setItem('LastName', LastName)
    //     // AsyncStorage.setItem('Address', Address)
    // }

    // chooseNewPhoto() 
    // {
    //     ImagePicker.openPicker({
    //             width: 300,
    //             height: 300,
    //             cropping: true,
    //             cropperCircleOverlay:true,
    //         })
    //         .then(i => 
    //                     {
    //                         this.setState({ AddPhotoOptions:false, EditPhotoOptions: false , image: i.path })  
    //                     }).catch(e => {
    //                             alert(e)
    //                             });
    // }

//------------->>>Controllers/Functions END------------>>>>

    render() {
        return(
            <View style={styles.container}>
                {/* <Avatar
                    xlarge
                    rounded
                    source={{ uri: this.state.image }}
                    onPress={() => this.setState({ AddPhotoOptions : true  })}
                    activeOpacity={0.7}
                    containerStyle={ styles.Avatar }
                />  
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 2 }}>
                        <TextField
                            name = "FirstName"
                            label = 'First Name'
                            labelHeight = { 16 }
                            autoCorrect = { false }
                            textColor =  { Colors.BLACK }
                            baseColor = { Colors.BLACK }
                            focus
                            withRef
                            ref = { (componentRef) => this.FirstName = componentRef }
                            refField = "FirstName"
                            onSubmitEditing = { () => { this.LastName.getRenderedComponent().refs.LastName.focus() } }
                            component = { ReduxFormTextField }
                            returnKeyType = { 'next' }
                            onChangeText = { (firstName) => {this.setState({ firstName: firstName })} }
                            validate={ [ RequiredValidation ] }
                        />

                    </View>
                    <View style={{ flex: 2, marginLeft: 8 }}>
                        <TextField
                            name = "LastName"
                            label = 'Last Name'
                            labelHeight = { 16 }
                            textColor =  { Colors.BLACK }
                            baseColor = { Colors.BLACK }
                            focus
                            withRef
                            ref = { (componentRef) => this.LastName = componentRef }
                            refField = "LastName"
                            autoCorrect = { false }
                            onSubmitEditing = { () => { this.Address.getRenderedComponent().refs.Address.focus() } }
                            component = { ReduxFormTextField }
                            returnKeyType = { 'next' }
                            value = { this.state.lastName }
                            onChange = { (event, lastName) => this.setState({ lastName: lastName }) }
                            validate={ [ RequiredValidation ] }
                        />
                    </View>
                </View>
                <View>
                    <TextField
                        name = "Address"
                        label = 'Address'
                        labelHeight = { 16 }
                        autoCorrect = { false }
                        textColor =  { Colors.BLACK }
                        baseColor = { Colors.BLACK }
                        focus
                        withRef
                        ref = { (componentRef) => this.Address = componentRef }
                        refField = "Address"
                        //onSubmitEditing = { () => { this.Password.getRenderedComponent().refs.Password.focus() } }
                        component = { ReduxFormTextField }
                        returnKeyType = { 'next' }
                        value = { this.state.address }
                        onChange = { (event, address) => this.setState({ address: address }) }
                        validate={ [ RequiredValidation ] }
                    />
                </View>
                <View style={styles.SavebuttonView}>
                   
                </View>
                <View style={styles.buttonView}>
                    <View style={styles.roundButtonStyle}>
                        <RoundedButton
                            title= "SAVE"
                            onPress={ () => this.onSavePress() }
                        />
                    </View>
                    <SimpleButton
                        title= "LOGOUT"
                        onPress={ () => this.onLogoutSubmit() }
                    />
                </View>

                <Alert 
                    visible={this.state.AddPhotoOptions}
                    title = {'Change Photo'}
                    onPress= {()  =>  { this.chooseNewPhoto() }}
                    ButtonTitle = { 'Open Gallery' }    
                />
                { this.state.isLoading ? <LoadWheel /> : null } */}
            </View>    
        )
    }
}

//----->>>Props Connection------>>>
const mapStateToProps = ( state ) => {
    return {
        settings : state.Settings
    }
  }

//--->>>Redux Connection------>>>
  export default connect(mapStateToProps ,{ logOutRequest })(SettingsScreen);

//====STYLES DECLARATION====//
const styles = StyleSheet.create({
    container: {
        flex:1,
        //backgroundColor:'green'
    },
    
    buttonView: {
        flex:1,
        justifyContent: 'flex-end',
        alignSelf: 'center',
        marginBottom: 15,
        alignItems: 'flex-end'
    },
    roundButtonStyle: {
        marginBottom:30
    },
    Avatar: {
        alignSelf: 'center',
        margin: 30
    }
})
