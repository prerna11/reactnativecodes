 //LIBRARIES
 import { StackNavigator } from 'react-navigation'
 //ASSETS
 import SettingsScreen from './SettingsScreen/View'


 //=========STACK DECLARATION FOR MAINTAB 3 SCREENS===========//

 
 const SettingsTabNav = StackNavigator({
    Screen1 : { screen : SettingsScreen  }
 });

 export default SettingsTabNav;