import React from 'react'
import {  Button } from 'react-native-elements';
import { StyleSheet } from 'react-native'

export const SimpleButton = ({ onPress,title, simpleButtonStyle }) => {
    return(
        <Button
            title={title}
            onPress={onPress}
            titleStyle={styles.SimpleButtonTitle}
            buttonStyle={[styles.SimpleButtonStyle,simpleButtonStyle]}
            containerStyle={styles.ButtonContainer}
        />
    )
}

export const RoundedButton = ({ onPress, title, roundedButtonStyle }) => {
    return(
        <Button
            rounded={true}
            title={title}
            onPress={onPress}
            titleStyle={styles.RoundedButtonTitle}
            buttonStyle={[styles.RoundedButtonStyle,roundedButtonStyle]}
            containerStyle={styles.ButtonContainer}
        />
    )
}

export const ButtonwIcon = ({ onPress, title, buttonwIconStyle }) => {
    return(
        <Button
            // rounded={true}
            //icon={{ name:'envira', type: 'font-awesome' }}
            leftIcon={{ name:'envira', type: 'font-awesome' }}
            title={title}
            onPress={onPress}
            titleStyle={styles.ButtonwIconTitle}
            buttonStyle={[styles.ButtonwIconStyle,buttonwIconStyle]}
            containerStyle={styles.ButtonContainer}
        />
    )
}

const styles = StyleSheet.create({
    ButtonContainer: {
        marginTop: 20
    },
    SimpleButtonTitle: {
        fontWeight: "700"
    },
    SimpleButtonStyle: {
        backgroundColor: "rgba(92, 99,216, 1)",
        width: 300,
        height: 45,
        borderColor: "transparent",
        borderWidth: 0,
        borderRadius: 5
    },
    RoundedButtonTitle: {
        fontWeight: "700"
    },
    RoundedButtonStyle: {
        backgroundColor: "rgba(92, 99,216, 1)",
        width: 300,
        height: 45,
        borderColor: "transparent",
    },
    ButtonwIconTitle: {
        fontWeight: "700"
    },
    ButtonwIconStyle: {
        backgroundColor: "rgba(92, 99,216, 1)",
        width: 300,
        height: 45,
        borderColor: "transparent",
        borderWidth: 0,
        borderRadius: 5
    }
})

//--->>For more customize buttons refer ->> react-native-elements---->>>>