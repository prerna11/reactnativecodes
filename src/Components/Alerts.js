//LIBRARIES
import React from 'react';
import { View, Text, Dimensions, StyleSheet } from 'react-native'
import Modal from 'react-native-modal'
//ASSETS
import { SimpleButton } from './Buttons'

const { width,height } = Dimensions.get('window')

export const Alert = ({ visible, title, ButtonTitle, onPress, message, children, titleStyle, MessageStyle, viewStyle, onModalHide }) => {
    return (
        <Modal
            transparent = { true }
            isVisible = { visible }
            avoidKeyboard = { true }
            style = { styles.Alert }
            onModalHide = { onModalHide }
            onRequestClose = { () => {alert( "Modal has been closed." )} }
            animationIn = { 'fadeIn' }
            animationOut = { 'fadeOut' } 
        >
            <View style = { styles.container }>
                <View style={[ styles.contentView, viewStyle ]}>
                    <Text style={[ styles.title, titleStyle ]}>{ title }</Text>
                    <Text style={[ styles.message, MessageStyle ]}>{ message }</Text>
                    <SimpleButton
                        title= { ButtonTitle }
                        onPress={ onPress }   
                        simpleButtonStyle= { styles.ButtonStyle }
                    />
                    { children }
                </View>
            </View>
        </Modal>
        
    )
}

const styles = StyleSheet.create({
    Alert: {
        borderWidth:2,
        width:width-0,
        height:height,
        alignSelf:'center',
        margin: 0
    },
    container: {
        flex:1, 
        backgroundColor:'rgba(0, 0, 0, 0.7)',
        justifyContent:'center',
        alignItems:'center' 
    },
    contentView: {
        backgroundColor:'white',
        width:300,
        padding:20,
        borderRadius:5 
    },
    title: {
        textAlign:'center',
        margin:10, 
        fontSize:24
    },
    message: {
        textAlign:'center', 
        margin:10, 
        fontSize:16, 
        color: 'grey' 
    },
    ButtonStyle: {
        width: 250,
        alignSelf:'center'
    }
})