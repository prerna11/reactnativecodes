
//-->>Field is required--->>>
export const RequiredValidation = ( value, allValues, props, name ) => {
    return (
        value ? undefined : ( name + ' is required' )
    )
}
//-->>Email is required--->>>
export const RequiredEmailValidation = ( value, allValues, props, name ) => {
    return (
        value ? undefined : ('Email is required')
    )
}

//-->> Validation for Email --->>>
export const EmailValidation = value => {
    return (
        value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,5}$/i.test(value) ? 'Invalid email address' : undefined
    )
}
//-->> Validation for Email --->>>
export const PasswordValidation = ( value, allValues, props, name ) => {
    return (
        ( value.length < 6 ) ? ( name + ' does not reach minimum character count' ) : undefined
    )
}