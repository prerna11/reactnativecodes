//LIBRARIES
import React,{ Component } from 'react';
import { TextField } from 'react-native-material-textfield'

//====CLASS DECLARATION====//
class ReduxFormTextField extends Component {

    render(){
        return (
            <TextField
                {...this.props.input}
                name = { this.props.name }
                label = { this.props.label }
                baseColor = {this.props.baseColor}
                containerStyle = { [ { flex:1 }, this.props.containerStyle ] }
                textColor = {this.props.textColor}
                labelHeight = { this.props.labelHeight }
                prefix = { this.props.prefix }
                suffix = { this.props.suffix }
                characterRestriction = { this.props.characterRestriction }
                multiline = { this.props.multiline }
                maxLength = { this.props.maxLength }
                keyboardType = { this.props.keyboardType }
                labelHeight = { this.props.labelHeight }
                labelPadding = { 0 }
                inputContainerPadding = { this.props.inputContainerPadding }
                tintColor = {this.props.tintColor}
                autoCorrect = { false }
                autoCapitalize = { 'none' }
                renderAccessory = { this.props.renderAccessory }
                onSubmitEditing = { this.props.onSubmitEditing }
                onChangeText = { this.props.onChange }
                clearButtonMode = { this.props.clearButtonMode }
                ref = { this.props.refField }
                value = { this.props.value }
                disabled = { this.props.disabled }
                caretHidden = { this.props.caretHidden }
                disabledLineType = { this.props.disabledLineType }
                returnKeyType = { this.props.returnKeyType }
                error = { this.props.meta.touched && this.props.meta.error ? this.props.meta.error : null }
                secureTextEntry = { this.props.secureTextEntry }
                editable = { this.props.editable }
            />
        )
    }
}

export { ReduxFormTextField }; 