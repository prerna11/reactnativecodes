import Colors from './GlobalColors';
import Images from './Images';
import Fonts from './Fonts'
import Matrics from './Matrics'

export { Colors, Images, Fonts, Matrics };