import { scale } from './Helpers/function';

const type ={
  base : 'Montserrat-Regular',
  bold : 'Montserrat-Bold'
}
const size = {
  h1: scale(38),
  h2: scale(34),
  h3: scale(30),
  h4: scale(26),
  h5: scale(20),
  h6: scale(19),
  input: scale(18),
  large: scale(18),
  regular: scale(16),
  medium: scale(14),
  small: scale(12),
  tiny: scale(8.5),
};

export default {size, type}